import { Author } from '../author';
import { Component, OnInit, Input } from '@angular/core';
import { AuthorService } from '../authors.service';
import { AuthorDetailsComponent } from '../author-details/author-details.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from "rxjs";

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {

  authors: Observable<Author[]>;

  constructor(private authorService: AuthorService,
    private router: Router) {}


    ngOnInit() {
      this.reloadData();
    }
  
    reloadData() {
      this.authors = this.authorService.getAuthorsList();
    }
    deleteAuthor(id: number) {
      this.authorService.deleteAuthor(id)
        .subscribe(
          data => {
            console.log(data);
            this.reloadData();
          },
          error => console.log(error));
    }
  
    authorDetails(id: number){
      this.router.navigate(['details', id]);
    }

    updateAuthor(id: number){
      this.router.navigate(['update', id]);
    }
  }


  