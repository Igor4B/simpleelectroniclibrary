import {BookDetailsComponent} from '../book-details/book-details.component';
import {Observable, from} from "rxjs";
import {BookService} from "../book.service";
import {Book} from "../book";
import {Component,OnInit} from "@angular/core";
import {Router} from "@angular/router";

@Component({
 selector: "app-book-list",
 templateUrl: "./book-list.component.html",
 styleUrls: ["./book-;ist.component.css"]
})
export class BookListComponenet implements OnInit {
  books : Observable<Book[]>;

  constructor (private bookService: BookService,
    private router : Router) {}

  ngOnInit(){
    this.reloadData();
  }
  reloadData() {
    this.books = this.bookService.getBooksList();
  }
  
  deleteBook(id: number) {
    this.bookService.deleteBook(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  bookDetails(id: number){
    this.router.navigate(['details', id]);
  }
}