export class Book {
    id:number;
    title: string;
    edition : number;
    yearofpublish: number;
    isbn: string;
}