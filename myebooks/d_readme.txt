
This is a very simple example for architecture layering for REST API with Spring and Hibernate. 

You can try it with following curl examples:


- find all
curl http://localhost:8080/v1/publishers

- find one
curl http://localhost:8080/v1/publishers/1

- save
curl -X POST -H "Content-Type: application/json" -d '{"id":3,"name":"Manning Publications Co."}' http://localhost:8080/v1/publishers

- delete
curl -X DELETE http://localhost:8080/v1/publishers/1





in src/main/resources
and in application.propertis
spring.datasource.username= you put your username here 
spring.datasource.password= you put your password here 

in order to make connecton with the database