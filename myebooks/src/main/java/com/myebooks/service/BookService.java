package com.myebooks.service;

import com.myebooks.model.Book;

public interface BookService extends GenericService<Book>{

}
