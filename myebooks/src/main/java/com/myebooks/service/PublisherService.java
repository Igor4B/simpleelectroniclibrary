package com.myebooks.service;

import com.myebooks.model.Publisher;

public interface PublisherService extends GenericService<Publisher> {

}
