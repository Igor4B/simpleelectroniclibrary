package com.myebooks.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myebooks.dao.AuthorRepository;
import com.myebooks.model.Author;
import com.myebooks.service.AuthorService;

@Service
public class AuthorServiceImpl implements AuthorService {

	@Autowired
	private AuthorRepository repository;

	@Override
	public List<Author> findAll() {
		return (List<Author>) repository.findAll();
	}

	@Override
	public Author findById(Integer id) {
		if (id == null) {
			return null;
		}
		Optional<Author> author = repository.findById(id);
		return author.orElse(null);

	}

	@Override
	public Author save(Author author) {
		if (author == null) {
			return null;
		}
		author = repository.save(author);
		return author;

	}

	@Override
	public Author update(Author author) {
		return save(author);
	}

	@Override
	public void delete(Integer id) {
		repository.deleteById(id);

	}

}
