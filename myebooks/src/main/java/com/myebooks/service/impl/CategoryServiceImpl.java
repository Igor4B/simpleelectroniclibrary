package com.myebooks.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myebooks.dao.CategoryRepository;
import com.myebooks.model.Category;
import com.myebooks.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository repository;

	@Override
	public List<Category> findAll() {
		return (List<Category>) repository.findAll();
	}

	@Override
	public Category findById(Integer id) {
		if (id == null) {
			return null;
		}
		Optional<Category> category = repository.findById(id);
		return category.orElse(null);
	}

	@Override
	public Category save(Category category) {
		if (category == null) {
			return null;
		}
		category = repository.save(category);
		return category;
	}

	@Override
	public Category update(Category category) {
		return save(category);
	}

	@Override
	public void delete(Integer id) {
		repository.deleteById(id);

	}

}
