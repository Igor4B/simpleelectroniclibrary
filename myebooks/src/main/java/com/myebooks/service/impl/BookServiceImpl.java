package com.myebooks.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myebooks.dao.BookRepository;
import com.myebooks.model.Book;
import com.myebooks.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository repository;

	@Override
	public List<Book> findAll() {
		return (List<Book>) repository.findAll();
	}

	@Override
	public Book findById(Integer id) {
		if (id == null) {
			return null;
		}
		Optional<Book> book = repository.findById(id);
		return book.orElse(null);
	}

	@Override
	public Book save(Book book) {
		if (book == null) {
			return null;
		}
		book = repository.save(book);
		return book;
	}

	@Override
	public Book update(Book book) {
		return save(book);

	}

	@Override
	public void delete(Integer id) {
		repository.deleteById(id);

	}

}
