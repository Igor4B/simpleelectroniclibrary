package com.myebooks.service;

import com.myebooks.model.Category;

public interface CategoryService extends GenericService<Category> {

}
