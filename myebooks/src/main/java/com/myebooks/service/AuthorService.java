package com.myebooks.service;

import com.myebooks.model.Author;

public interface AuthorService extends GenericService<Author> {

}
