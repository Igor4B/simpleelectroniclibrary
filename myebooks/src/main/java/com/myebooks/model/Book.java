package com.myebooks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "books")
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "title", nullable = false, length = 100)
	@NonNull
	private String title;

	@Column(name = "edition", nullable = false, length = 100)
	@NonNull
	private Integer edition;

	@Column(name = "year_of_publish", nullable = false, length = 100)
	@NonNull
	private Integer yearofpublish;

	@Column(name = "ISBN", nullable = false, length = 100)
	@NonNull
	private String isbn;

}
