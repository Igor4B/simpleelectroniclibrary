package com.myebooks.controller.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myebooks.dao.BookRepository;
import com.myebooks.exception.ResourceNotFoundException;
import com.myebooks.model.Book;


@RestController
@RequestMapping("api/v1")
public class BookController {

	@Autowired
	private BookRepository bookRepository;

	@GetMapping("/books")
	public List<Book> getAllAuthors() {
		return bookRepository.findAll();
	}

	@GetMapping("/books/{id}")
	public ResponseEntity<Book> findBookById(@PathVariable(value = "id") Integer Id) 
			throws ResourceNotFoundException {

		Book book = bookRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Book not found for this id :: " + Id));;

		return ResponseEntity.ok().body(book);
	}
	
	
	@PostMapping("/books")
	Book createOrSaveBooks(@RequestBody Book newBook) {
		return bookRepository.save(newBook);
	}

	@PutMapping("/books/{id}")
	public ResponseEntity<Book> updateBook(@PathVariable(value = "id") Integer Id,
			@Valid @RequestBody Book bookDetails) throws ResourceNotFoundException {
		Book book = bookRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Author not found for this id :: " + Id));

		book.setTitle(bookDetails.getTitle());
		book.setEdition(bookDetails.getEdition());
		book.setYearofpublish(bookDetails.getYearofpublish());
		book.setIsbn(bookDetails.getIsbn());

		Book updatedBook = bookRepository.save(book);
		return ResponseEntity.ok(updatedBook);
	}

	@DeleteMapping("/books/{id}")
	public void delete(@PathVariable(value = "id") Integer id) {
		bookRepository.deleteById(id);
	}
}
