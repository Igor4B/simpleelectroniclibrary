package com.myebooks.controller.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myebooks.dao.PublisherRepository;
import com.myebooks.exception.ResourceNotFoundException;
import com.myebooks.model.Publisher;

@RestController
@RequestMapping("api/v1")
public class PublisherController {

	@Autowired
	private PublisherRepository publisherRepository;

	@GetMapping("/publishers")
	public List<Publisher> findAllPublishers() {
		return publisherRepository.findAll();
	}

	@GetMapping("/publishers/{id}")
	public ResponseEntity<Publisher> findById(@PathVariable(value = "id") Integer Id) 
	 	throws ResourceNotFoundException{

		Publisher publisher = publisherRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Publisher not found for this id :: " + Id));;
		
		return ResponseEntity.ok().body(publisher);
	}

	@PutMapping("/publisers/{id}")
	public ResponseEntity<Publisher> updatePublisher(@PathVariable(value = "id") Integer Id,
		   @Valid @RequestBody Publisher publisherDetails) throws ResourceNotFoundException {
		Publisher publisher = publisherRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Publisher not found for this id :: " + Id));

		publisher.setName(publisherDetails.getName());
		

		Publisher updatedPublisher = publisherRepository.save(publisher);
		return ResponseEntity.ok(updatedPublisher);
	}

	@DeleteMapping("/publishers/{id}")
	public void delete(@PathVariable(value = "id") Integer id) {
		publisherRepository.deleteById(id);
	}
}