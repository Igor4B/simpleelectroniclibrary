package com.myebooks.controller.rest;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myebooks.dao.AuthorRepository;
import com.myebooks.exception.ResourceNotFoundException;
import com.myebooks.model.Author;

@RestController
@RequestMapping("api/v1")
public class AuthorController {

	@Autowired
	private AuthorRepository authorRepository;

	@GetMapping("/authors")
	public List<Author> getAllAuthors() {
		return authorRepository.findAll();
	}

	@GetMapping("/authors/{id}")
	public ResponseEntity<Author> getEmployeeById(@PathVariable(value = "id") Integer Id)
			throws ResourceNotFoundException {
		Author author = authorRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Author not found for this id :: " + Id));
		return ResponseEntity.ok().body(author);
	}

	@PostMapping("/authors")
	Author createOrSaveAuthor(@RequestBody Author newAuthor) {
		return authorRepository.save(newAuthor);
	}

	@PutMapping("/authors/{id}")
	public ResponseEntity<Author> updateAuthor(@PathVariable(value = "id") Integer Id,
			@Valid @RequestBody Author authorDetails) throws ResourceNotFoundException {
		Author author = authorRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Author not found for this id :: " + Id));

		author.setFirstName(authorDetails.getFirstName());
		author.setLastName(authorDetails.getLastName());

		Author updatedAuthor = authorRepository.save(author);
		return ResponseEntity.ok(updatedAuthor);
	}
	
	@DeleteMapping("/authors/{id}")
	public void delete(@PathVariable(value = "id") Integer id) {
		authorRepository.deleteById(id);
	}
}
