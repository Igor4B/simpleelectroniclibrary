package com.myebooks.controller.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myebooks.dao.CategoryRepository;
import com.myebooks.exception.ResourceNotFoundException;
import com.myebooks.model.Category;

@RestController
@RequestMapping("api/v1")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("/categorys")
	public List<Category> findAllCategorys() {
		return categoryRepository.findAll();
	}

	@GetMapping("/categorys/{id}")
	public ResponseEntity<Category> findById(@PathVariable(value = "id") Integer Id) 
			throws ResourceNotFoundException {

		Category category = categoryRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Category not found for this id :: " + Id));
		;

		return ResponseEntity.ok().body(category);
	}

	@PostMapping("/categorys")
	Category createOrSaveCategory(@RequestBody Category newCategory) {
		return categoryRepository.save(newCategory);
	}

	@PutMapping("/categorys/{id}")
	public ResponseEntity<Category> updateCategory(@PathVariable(value = "id") Integer Id,
			@Valid @RequestBody Category categoryDetails) throws ResourceNotFoundException {
		Category category = categoryRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Author not found for this id :: " + Id));

		category.setCategoryName(categoryDetails.getCategoryName());
		

		Category updatedCategory = categoryRepository.save(category);
		return ResponseEntity.ok(updatedCategory);
	}

	@DeleteMapping("/categorys/{id}")
	public void delete(@PathVariable(value = "id") Integer id) {
		categoryRepository.deleteById(id);
	}
}
